# GDS2MAG
GDS2MAG reads GDS2 layouts and converts the layout information into the Magic (.mag) file format.

Example:
```bash
gds2mag --config example/example_config.toml \
    -i /path/to/some.gds \
    -o /path/to/output.mag
```

Note that the Polygons from the GDS2 are all converted to rectilinear rectangles.
Therefore, polygons with non-rectilinear lines cannot be converted.

Only flat cells (no hierarchy) are supported. Also, currently other GDS2 objects such as Text, Path, ... are not handled. 

## Install
```bash
git clone [this repo]
cd gds2mag
python3 setup.py install --user
```