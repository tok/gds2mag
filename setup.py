from setuptools import setup


def readme():
    with open("README.md", "r") as f:
        return f.read()


setup(name='gds2mag',
      version='0.0.0',
      description='GDS2 to Magic (.mag) converter.',
      long_description=readme(),
      long_description_content_type="text/markdown",
      keywords='gds gds2 magic mag converter klayout',
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3',
          'Development Status :: 3 - Alpha',
          'Topic :: Scientific/Engineering',
          'Topic :: Scientific/Engineering :: Visualization',
          'Topic :: Scientific/Engineering :: Electronic Design Automation (EDA)',
          'Programming Language :: Python :: 3'
      ],
      url='https://codeberg.org/tok/gds2mag',
      author='T. Kramer',
      author_email='code@tkramer.ch',
      license='GPLv3+',
      packages=['gds2mag'],
      include_package_data=True,
      entry_points={
          'console_scripts': [
              'gds2mag = gds2mag.standalone:main',
          ]
      },
      install_requires=[
          'klayout',
          'toml'
      ],
      zip_safe=False)
